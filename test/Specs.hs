module Main  where
import Test.Hspec
import Control.Exception (evaluate)
import Martyniuk09
import TestTemplate

main :: IO ()
main = tests

tests = hspec $ do 
    describe "7th" $ do
        subTest "" $ do
            showRE (simplify re5) `shouldBe` "(ab|)dd*"
            terminalStates  nda1  `shouldBe`  [2]
            isTerminal 2 nda1 `shouldBe`  True
            isTerminal 3 da4 `shouldBe` True
            isTerminal 5 nda2 `shouldBe` False
            transitionsFrom 5 ndaFigure `shouldBe`  [(5,7,Eps), (5,9,Eps)]
            transitionsFrom 10 nda3 `shouldBe` [(10,6, C 'b')]
            labels [(1,2,Eps)] `shouldBe` []
            labels (transitions nda3)  `shouldBe` [C 'a', C 'c', C 'b']
            stStep nda1 4 Eps `shouldBe`  [9, 11]
            setStep nda4 ([2,4,5]) (C 'a') `shouldBe`  [6,2] 
            closure ndaTest ( [2])  `shouldBe`  [1..5]
            accepts ndaFigure "ac" `shouldBe` True
            accepts ndaFigure "aac" `shouldBe` True
            accepts ndaFigure "ad" `shouldBe` False
            makeNDA reFigure `shouldBe` ndaFigure 
            makeNDA re1 `shouldBe` nda1
            makeNDA re4 `shouldBe` nda4  
            -- makeDA ndaFigure `shouldBe` daFigure
            -- makeDA nda1 `shouldBe` da1
            -- makeDA nda3 `shouldBe` da3  

 


