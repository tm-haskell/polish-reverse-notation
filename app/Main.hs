module Main where
import System

s = "1 + 2 * 3" -- 7
s2 = "( 1 + 2 ) * 3" -- 9
notSolvable = "x - ( 4 + 3 ) * 2"


ex str = let stack = prnStack str in
    "For input \'" ++ str ++ "\' Stack is: \'"++ stack ++ "\', solved: " ++ solve stack 

main = do
    print $ ex s
    print $ ex s2
    print $ ex notSolvable

