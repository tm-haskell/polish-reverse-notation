{-# OPTIONS_GHC -Wall #-}
module System where
import Text.Printf
import Data.List
import Data.Maybe

validSymbols = "1234567890.,*+-/^lnsum "

isSolvable :: String -> Bool
isSolvable s =
    let findRes = find (\x -> not $ elem x validSymbols) s in
        case (findRes) of
            Just _ -> False
            Nothing -> True




---------------------------------------- parse -----------------------------------------
-- must be a valid polish reverse notation string
-- must contain only constants - no variables
solve :: String -> String
solve str
    | not $ isSolvable str = str
    | otherwise = show res
        where res = head . foldl solveFold [] $ words str

solveFold :: (Floating a, Read a) => [a] -> [Char] -> [a]
solveFold (x:y:ys) "*" = (x * y):ys  
solveFold (x:y:ys) "+" = (x + y):ys  
solveFold (x:y:ys) "-" = (y - x):ys  
solveFold (x:y:ys) "/" = (y / x):ys  
solveFold (x:y:ys) "^" = (y ** x):ys  
solveFold (x:xs) "ln" = log x:xs  
solveFold xs "sum" = [sum xs]  
solveFold xs numberString = read numberString:xs

---------------------------------------- create -----------------------------------------
 
prec :: String -> Int
prec "^" = 4
prec "*" = 3
prec "/" = 3
prec "+" = 2
prec "-" = 2
 
leftAssoc:: String -> Bool
leftAssoc "^" = False
leftAssoc _ = True
 
isOp (t:[]) = t `elem` "-+/*^"
isOp _ = False

simSYA :: [String] -> [([String], [String], String)]
simSYA xs = final ++ [lastStep]
  where final = scanl f ([],[],"") xs
        lastStep = (\(x,y,_) -> (reverse y ++ x, [], "")) $ last final
        f (out,st,_) t | isOp t    =
                         (reverse (takeWhile testOp st) ++ out
                         , (t:) $ (dropWhile testOp st), t)
                       | t == "("  = (out, "(":st, t)
                       | t == ")"  = (reverse (takeWhile (/="(") st) ++ out,
                                     tail $ dropWhile (/="(") st, t)
                       | otherwise = (t:out, st, t)
          where testOp x = isOp x && (leftAssoc t && prec t == prec x
                                      || prec t < prec x)
 

stackHistory :: String -> [[String]]
stackHistory str = 
    let tuples = simSYA $ words str in
        map (\(x, _, _) -> x) tuples


prnStack :: String -> String
prnStack str = unwords $ reverse $ lastStack
    where lastStack = head $ reverse $ stackHistory str

